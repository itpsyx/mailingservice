namespace MailingService.Enums;

public enum MessageStatus
{
    Ok,
    InQueue,
    Error,
}