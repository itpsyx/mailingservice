namespace MailingService.DTO;

public class MessageRequestDto
{
    public string Subject { get; set; }
    public string Body { get; set; }
    public List<string> Recipients { get; set; }
}