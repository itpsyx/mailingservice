using MailingService.Data;
using MailingService.Models;
using Microsoft.EntityFrameworkCore;

namespace MailingService.Services;

public class MessageService : IMessageService
{
    private readonly MailingDbContext _context;

    public MessageService(MailingDbContext context)
    {
        _context = context;
    }

    /// <summary>
    /// Saves an <see cref="Message"/> instance in database.
    /// </summary>
    /// <param name="message"></param>
    public async Task AddMessageAsync(Message message)
    {
        await _context.Messages.AddAsync(message);
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Gets a list of all <see cref="Message"/> from database.
    /// </summary>
    public async Task<IEnumerable<Message>> GetAllMessagesAsync()
    {
        var mailList = await _context.Messages.ToListAsync();
        return mailList.AsEnumerable();
    }
}