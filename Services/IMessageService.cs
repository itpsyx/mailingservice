using MailingService.Models;

namespace MailingService.Services;

public interface IMessageService
{
    Task AddMessageAsync(Message message);
    Task<IEnumerable<Message>> GetAllMessagesAsync();
}