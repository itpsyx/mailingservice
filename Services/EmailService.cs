using System.Net;
using MailingService.DTO;
using MailingService.Settings;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;

namespace MailingService.Services;

public class EmailService : IEmailService
{
    private readonly SmtpSettings _smtpSettings;

    public EmailService(IOptions<SmtpSettings> smtpSettings)
    {
        _smtpSettings = smtpSettings.Value;
    }

    /// <summary>
    /// Sends an email.
    /// </summary>
    /// <param name="messageRequestDto">Mail request from request body.</param>
    public async Task SendEmailAsync(MessageRequestDto messageRequestDto)
    {
        using var email = new MimeMessage();
        email.Sender = MailboxAddress.Parse(_smtpSettings.Mail);
        email.To.AddRange(messageRequestDto.Recipients.Select(MailboxAddress.Parse));
        email.Subject = messageRequestDto.Subject;
        var builder = new BodyBuilder();
        builder.TextBody = messageRequestDto.Body;
        email.Body = builder.ToMessageBody();

        using var smtp = new SmtpClient();
        await smtp.ConnectAsync(_smtpSettings.Host, _smtpSettings.Port, SecureSocketOptions.StartTls);
        await smtp.AuthenticateAsync(_smtpSettings.Username, _smtpSettings.Password);
        await smtp.SendAsync(email);
        await smtp.DisconnectAsync(true);
    }
}