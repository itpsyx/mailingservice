using MailingService.Data;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;

namespace MailingService.Services;

public class RecipientService : IRecipientService
{
    private readonly MailingDbContext _context;

    public RecipientService(MailingDbContext context)
    {
        _context = context;
    }
}