using MailingService.DTO;

namespace MailingService.Services;

public interface IEmailService
{
    Task SendEmailAsync(MessageRequestDto messageRequestDto);
}