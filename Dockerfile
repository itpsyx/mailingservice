﻿FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as build

WORKDIR /src
COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet build -o /app
RUN dotnet publish -o /publish

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine as runtime
COPY --from=build /publish /app
WORKDIR /app
EXPOSE 80
ENTRYPOINT [ "dotnet", "MailingService.dll" ]