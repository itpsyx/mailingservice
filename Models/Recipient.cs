namespace MailingService.Models;

public class Recipient
{
    public Guid Id { get; set; }
    public string Email { get; set; }
    public ICollection<Message> Messages { get; set; }
    public ICollection<MessageRecipient> MessageRecipients { get; set; }
}