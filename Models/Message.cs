using MailingService.Enums;

namespace MailingService.Models;

public class Message
{
    public Guid Id { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
    public DateTime DateSent { get; set; }
    public MessageStatus Status { get; set; }
    public string? ErrorMessage { get; set; }
    public ICollection<Recipient> Recipients { get; set; }
    public ICollection<MessageRecipient> MessageRecipients { get; set; }
}