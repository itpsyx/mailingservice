namespace MailingService.Models;

public class MessageRecipient
{
    public Guid MessageId { get; set; }
    public Guid RecipientId { get; set; }
    public Message Message { get; set; }
    public Recipient Recipient { get; set; }
}