using MailingService.Data;
using MailingService.Enums;
using MailingService.Services;
using MailingService.Settings;
using Microsoft.EntityFrameworkCore;
using Npgsql;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddNpgsqlDataSource(builder.Configuration.GetConnectionString("DefaultConnection"),
    dataSourceBuilder =>
    {
        dataSourceBuilder.MapEnum<MessageStatus>();
    });
builder.Services.AddDbContext<MailingDbContext>(options => options.UseNpgsql());
builder.Services.Configure<SmtpSettings>(builder.Configuration.GetSection("SmtpSettings"));
builder.Services.AddTransient<IEmailService, EmailService>();
builder.Services.AddTransient<IMessageService, MessageService>();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using var scope = app.Services.CreateScope();
var mailsContext = scope.ServiceProvider.GetRequiredService<MailingDbContext>();
mailsContext.Database.Migrate();
using var connection = (NpgsqlConnection)mailsContext.Database.GetDbConnection();
connection.Open();
connection.ReloadTypes();

app.Run();