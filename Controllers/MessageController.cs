using MailingService.Data;
using MailingService.DTO;
using MailingService.Enums;
using MailingService.Models;
using MailingService.Services;
using Microsoft.AspNetCore.Mvc;

namespace MailingService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class MessageController : ControllerBase
{
    private readonly IMessageService _messageService;
    private readonly IEmailService _emailService;

    public MessageController(IMessageService messageService, IEmailService emailService)
    {
        _messageService = messageService;
        _emailService = emailService;
    }

    /// <summary>
    /// Gets a list of all emails with additional information.
    /// </summary>
    [HttpGet]
    public async Task<IEnumerable<Message>> Get()
    {
        return await _messageService.GetAllMessagesAsync();
    }
    
    /// <summary>
    /// Sends an email and saves all the information in the database.
    /// </summary>
    /// <param name="messageRequestDto">Mail request from request body.</param>
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] MessageRequestDto messageRequestDto)
    {
        var mail = new Message()
        {
            Id = Guid.NewGuid(),
            Subject = messageRequestDto.Subject,
            Body = messageRequestDto.Body,
            Recipients = null,
            DateSent = DateTime.UtcNow,
        };
        try
        {
            await _emailService.SendEmailAsync(messageRequestDto);
            mail.Status = MessageStatus.Ok;
            return Ok();
        }
        catch (Exception ex)
        {
            mail.Status = MessageStatus.Error;
            mail.ErrorMessage = ex.Message;
            return BadRequest();
        }
        finally
        {
            await _messageService.AddMessageAsync(mail);
        }
    }
}