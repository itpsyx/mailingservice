using MailingService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MailingService.Configurations;

public class RecipientConfiguration : IEntityTypeConfiguration<Recipient>
{
    public void Configure(EntityTypeBuilder<Recipient> builder)
    {
        builder.ToTable("recipients");
        builder.HasKey(r => r.Id);
        builder.HasIndex(r => r.Email)
            .IsUnique();
        
        builder.Property(r => r.Id)
            .ValueGeneratedOnAdd()
            .HasColumnName("recipient_id");
        builder.Property(r => r.Email)
            .IsRequired()
            .HasColumnName("email");

    }
}