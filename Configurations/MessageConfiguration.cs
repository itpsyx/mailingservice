using MailingService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MailingService.Configurations;

public class MessageConfiguration : IEntityTypeConfiguration<Message>
{
    public void Configure(EntityTypeBuilder<Message> builder)
    {
        builder.ToTable("messages");
        builder.HasKey(m => m.Id);
        
        builder.Property(m => m.Id)
            .ValueGeneratedOnAdd()
            .HasColumnName("message_id");
        builder.Property(m => m.Subject)
            .IsRequired()
            .HasColumnName("subject");
        builder.Property(m => m.Body)
            .IsRequired()
            .HasColumnName("body");
        builder.Property(m => m.DateSent)
            .IsRequired()
            .HasColumnName("date_sent");
        builder.Property(m => m.Status)
            .IsRequired()
            .HasColumnName("status");
        builder.Property(m => m.ErrorMessage)
            .HasColumnName("error_message");

        builder.HasMany(m => m.Recipients)
            .WithMany(r => r.Messages)
            .UsingEntity<MessageRecipient>();
    }
}