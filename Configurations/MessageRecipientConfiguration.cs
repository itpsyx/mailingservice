using MailingService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MailingService.Configurations;

public class MessageRecipientConfiguration : IEntityTypeConfiguration<MessageRecipient>
{
    public void Configure(EntityTypeBuilder<MessageRecipient> builder)
    {
        builder.ToTable("message_recipient");
        builder.HasKey(mr => new { mr.MessageId, mr.RecipientId });
        
        builder.Property(mr => mr.MessageId)
            .HasColumnName("message_id");
        builder.Property(mr => mr.RecipientId)
            .HasColumnName("recipient_id");
    }
}