using System.Reflection;
using MailingService.Enums;
using MailingService.Models;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Tls;

namespace MailingService.Data;

public class MailingDbContext : DbContext
{
    public MailingDbContext(DbContextOptions<MailingDbContext> options)
        : base(options)
    {

    }

    public DbSet<Message> Messages { get; set; }
    public DbSet<Recipient> Recipients { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasPostgresEnum<MessageStatus>();
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}