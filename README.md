## Mailing Service
### To start a service in a docker container, enter the following command in the terminal with the appropriate smtp server values:
#### for cmd:
set "MAIL=" && set "USERNAME=" && set "PASSWORD=" && set "HOST=" && set "PORT=" && docker compose build && docker compose up
#### for bash:
export MAIL="" USERNAME="" PASSWORD="" HOST="" PORT="" && docker-compose build && docker-compose up